var win = 0;
var lose = 0;
var draw = 0;



//选择出拳
function doChange(){
    var select = document.getElementById('select').value;//用户当前选的是啥值
    var current = document.getElementById('current');
    if(select==1){
        current.src='images/st.jpg';
    }else if(select==2){
        current.src='images/jd.jpg';
    } else if(select==3){
        current.src='images/bu.jpg';
    }else{
        current.src='images/zb.jpg';
    }
}
//确认出拳
function doclick(){
    var select = document.getElementById('select').value;//用户当前选的是啥
    var you = document.getElementById('you');
    var computer = document.getElementById('computer');
    //你出拳
    if(select==1){
        you.src='images/st.jpg';
    }else if(select==2){
        you.src='images/jd.jpg';
    } else if(select==3){
        you.src='images/bu.jpg';
    }else{
        alert('请先选择再出拳！');
        return; //停止执行
    }
    //电脑出拳
    var x = parseInt(Math.random()*3+1);//产生[0,3]的随机数
    if(x==1){
        computer.src='images/st.jpg';
    }else if(x==2){
        computer.src='images/jd.jpg';
    }else if(x==3){
        computer.src='images/bu.jpg';
    }
    //判断结果
    var result = document.getElementById('result');
    var minus = select - x;
    if(minus==0){
        result.innerText = '平局';
        draw++;
    }else if(minus==-1 || minus==2){
        result.innerText = '胜利';
        win++;
    }else{
        result.innerText = '失败';
        lose++;
    }
    //显示结果
    document.getElementById('total').innerText = win + lose + draw;
    document.getElementById('win').innerText = win ;
    document.getElementById('lose').innerText = lose ;
    document.getElementById('draw').innerText = draw ;
} 